const path = require('path')
const htmlWebpackPlugin = require('html-webpack-plugin')
const copyWebpackPlugin = require('copy-webpack-plugin')
const webpack = require('webpack')
const cesiumSource = 'node_modules/cesium/Source'
const cesiumBuild = 'node_modules/cesium/Build/Cesium'
const cesiumWorkers = '../Build/Cesium/Workers'

module.exports = {
  context: __dirname,
  entry: {
    app: './src/index.js',
  },
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, 'dist'),
    // Needed to compile multiline strings in cesium(?)
    // 修改输出bundle中每行的前缀
    sourcePrefix: '',
  },
  resolve: {
    // 创建模块import/export的别名
    alias: {
      // 源码
      cesium: path.resolve(__dirname, cesiumSource),
    },
    // 正常解析失败时，重定向模块请求
    fallback: { https: false, zlib: false, http: false, url: false },
    // 解析目录时要使用的文件名
    mainFiles: ['module', 'main', 'cesium'],
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|gif|jpg|jpeg|svg|xml|json)$/,
        use: ['url-loader'],
      },
    ],
  },
  plugins: [
    new htmlWebpackPlugin({
      template: 'src/index.html',
    }),
    // 拷贝静态文件到dist
    new copyWebpackPlugin({
      patterns: [
        // npm下载的cesium源码存在部分缺失，build版本为完整版
        { from: path.join(cesiumSource, cesiumWorkers), to: 'Workers' },
        { from: path.join(cesiumSource, 'Assets'), to: 'Assets' },
        { from: path.join(cesiumSource, 'Widgets'), to: 'Widgets' },
        { from: path.join(cesiumSource, 'ThirdParty'), to: 'ThirdParty' },
      ],
    }),
    new webpack.DefinePlugin({
      CESIUM_BASE_URL: JSON.stringify(''),
    }),
  ],
  mode: 'development',
  devtool: 'eval',
}

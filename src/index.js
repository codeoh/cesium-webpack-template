import {
  Ion,
  Viewer,
  createWorldTerrainAsync,
} from 'cesium'
import 'cesium/Widgets/widgets.css'
import './index.css'

// Cesium ion身份验证
Ion.defaultAccessToken = ``
const viewer = new Viewer('cesiumContainer', {
  terrainProvider: await createWorldTerrainAsync({ requestVertexNormals: true, requestWaterMask: true, }),
})

